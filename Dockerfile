FROM python:3.6.5-alpine3.7

# System deps:

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  PIPENV_COLORBLIND=true \
  PIPENV_NOSPIN=true

RUN apk update \
    && apk --no-cache add \
      bash \
      build-base \
      curl \
      # gettext \
      git \
      # libffi-dev \
      linux-headers \
      postgresql-dev
      # libpq-dev
      # tini

RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN pip install pipenv \
  && pipenv install --system --deploy --ignore-pipfile
