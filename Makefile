MANAGE_PY = $(PYTHON) $(PWD)/manage.py

runserver:
	$(MANAGE_PY) runserver 0.0.0.0:8000
migrate:
	$(MANAGE_PY) migrate --noinput
migrations:
	$(MANAGE_PY) makemigrations $(ARGS)
docker-ssh:
	docker-compose exec django bash
make shell:
	$(MANAGE_PY) shell_plus --ipython

docker-sync-stack: docker-compose.override.yml
	docker-sync-stack start

.PHONY:						\
	runserver				\
	migrate					\
	makemigrations				\
	docker-ssh				\
