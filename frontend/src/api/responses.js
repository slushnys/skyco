import session from './session'

export default {
  createResponse(payload) {
    return session.post('/api/v1/responses', payload)
  },
  getResponse(id) {
    return session.get(`/api/v1/responses/${id}`)
  },
  update(payload) {
    return session.post(`api/v1/responses/${payload.id}/update`, payload)
  },
  getPersonal() {
    return session.get('api/v1/responses/personal')
  },
  acceptResponse(payload) {
    return session.post(`api/v1/responses/${payload.id}/accept`, payload)
  }
}
