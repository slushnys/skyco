import session from './session'

const api_endpoint = '/api/v1'

export default {
  getUser() {
    return session.get('/api/v1/users/me')
  },
  getPersonalCompany() {
    return session.get('/api/v1/company/me')
  },
  createCompany(payload) {
    return session.post('/api/v1/company', payload)
  },
  updateCompany(payload) {
    return session.patch(`/api/v1/company/${payload.id}`, payload)
  },
  getContracts() {
    return session.get(`${api_endpoint}/contracts`)
  },
  getContract(id) {
    return session.get(`${api_endpoint}/contracts/${id}`)
  },
  createContract(payload) {
    return session.post(`${api_endpoint}/contracts`, payload)
  },
  updateContract(payload) {
    return session.patch(`${api_endpoint}/contracts/${payload.id}`, payload)
  }
}
