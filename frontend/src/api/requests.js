import session from './session'

export default {
  createRequest(payload) {
    return session.post('/api/v1/requests', payload)
  },
  getRequest(requestId) {
    return session.get(`/api/v1/requests/${requestId}`)
  },
  getPersonal() {
    return session.get('api/v1/requests/personal')
  },
  getAll() {
    return session.get('api/v1/requests')
  },
  getResponses(requestId) {
    return session.get(`api/v1/requests/${requestId}/responses`)
  },
  deleteRequest(requestId) {
    return session.delete(`api/v1/requests/${requestId}`)
  }
}
