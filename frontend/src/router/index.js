import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/Login';
import {
  RequestCreate,
  RequestIncomingList,
  RequestPersonalList,
  RequestResponsesList,

  // Responses
  ResponseList,
  ResponseCreate,
  ResponseDetail,
  ResponsePersonalList,

  // Contracts
  ContractList,
  ContractCreate,
  ContractDetailEditor,

  CompanyDetailEditor
} from '@/components/'
import Register from '../views/Register';

import store from '../store';

Vue.use(Router)

const requireAuthentication = (to, from, next) => {
  if (!store.getters['auth/isAuthenticated']) {
    next('/login')
  } else {
    next()
  }
}

const requireUnauthenticated = (to, from, next) => {
  store.dispatch('auth/initialize')
    .then(() => {
      if (store.getters['auth/isAuthenticated']) {
        next('/');
      } else {
        next();
      }
    });
};

const redirectLogout = (to, from, next) => {
  store.dispatch('auth/logout')
    .then(() => next('/login'));
};

let authenticatedRoute = (path, name, component, customizations) => {
  return {
    path: path,
    name: name,
    component: component,
    beforeEnter: requireAuthentication,
    ...customizations
  }
}

export default new Router({
  routes: [
    authenticatedRoute('/', 'IncomingRequests', RequestIncomingList, {
      meta: {
        title: 'Incoming requests',
        subtitle: 'ACMI requests coming from other companies.',
        button: {
          name: 'Create new request',
          path: '/requests/create'
        },
        metaTags: {}
      }
    }),
    authenticatedRoute('/requests/create', 'RequestCreate', RequestCreate, {
      meta: {
        title: 'Create request',
        subtitle: 'Fill in the details to create a request that other companies could see.',
        button: null,
        metaTags: {}
      }
    }),
    authenticatedRoute(
      '/requests/personal',
      'RequestPersonalList',
      RequestPersonalList,
      {
        meta: {
          title: 'Your requests',
          subtitle: 'The ACMI requests which you have created',
          button: {
            name: 'Create new request',
            path: '/requests/create'
          },
          metaTags: {}
        }
      }
    ),
    authenticatedRoute('/requests/:id/respond', 'ResponseCreate', ResponseCreate, {
      props: true,
      beforeEnter: (to, from, next) => {
        next()
      }
    }),
    authenticatedRoute('/requests/:id/responses', 'RequestResponsesList', RequestResponsesList, {
      props: true,
      beforeEnter: (to, from, next) => {
        next()
      },
      meta: {
        title: 'Responses to your request',
        subtitle: 'All responses from other companies related to your request.',
        button: null,
        metaTags: {}
      }
    }),
    authenticatedRoute('/responses/personal', 'ResponsePersonalList', ResponsePersonalList, {
      props: false,
      meta: {
        title: 'Your responses',
        subtitle: 'List of responses that you have made to others requests.',
        button: null,
        metaTags: {}
      }
    }),
    authenticatedRoute('/responses/:id', 'ResponseDetail', ResponseDetail, {
      props: true,
      meta: {
        title: 'Response detail',
        subtitle: 'Detailed information of a response to your request given a contract.',
        button: null,
        metaTags: {}
      }
    }),

    {
      path: '/requests/personal/:id',
      component: RequestPersonalList,
      beforeEnter: requireAuthentication,
      children:
      [
        {
          path: '/responses',
          component: ResponseList
        },
      ]
    },
    {
      path: '/register',
      component: Register,
    },
    {
      path: '/login',
      component: Login,
      beforeEnter: requireUnauthenticated,
      meta: {
        title: 'Login',
        subtitle: 'Authenticate youself to access the full posibilities of the platform.',
        button: null,
        metaTags: {}
      }
    },
    {
      path: '/logout',
      beforeEnter: redirectLogout,
    },
    {
      path: '/settings/contracts',
      component: ContractList,
      name: 'contractsList',
      meta: {
        title: 'Contracts',
        subtitle: 'Your contracts.',
        button: {
          path: '/settings/contracts/create',
          name: 'Create new contract'
        },
        metaTags: {}
      }
    },
    {
      path: '/settings/contracts/:id',
      component: ContractDetailEditor,
      name: 'contractsEditor',
      props: true,
      meta: {
        title: 'Contract details',
        subtitle: 'Editor for you to create and edit your contract.',
        button: null,
        metaTags: {}
      }
    },
    {
      path: '/settings/company',
      component: CompanyDetailEditor,
      name: 'companyEditor',
      props: false,
      meta: {
        title: 'Company information',
        subtitle: 'Information about your company that will be used when creating requests and responding to them.',
        button: null,
        metaTags: {}
      }
    },
    {
      path: '/settings',
      children: [
        {
          path: 'contract/create',
          name: '',
          component: ContractCreate
        },
        { path: 'company', component: ContractCreate }
      ]
    },
  ]
})
