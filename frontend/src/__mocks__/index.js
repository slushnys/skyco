import moment from 'moment'

export const contractsMock = [
  {id: 1, name: 'SkyCo Contract', created_at: moment()},
  {id: 2, name: '[SkyCo] Summer tarrif and rules contract', created_at: moment()},
  {id: 3, name: '[SkyCo] Transatlantic flights contract', created_at: moment()},
]
