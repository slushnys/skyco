import _ from 'lodash'
import requests from '../api/requests'

import {
  REQUEST_CREATE_BEGIN,
  REQUEST_CREATE_FAILURE,
  REQUEST_RETRIEVE_PERSONAL,
  ROUTE_ADD,
  ROUTE_DELETE
} from './types'

const state = {
  routes: {},
  creating: false,
  successMessage: '',
  errors: {},
  error: false,
  personalRequests: {},
  incomingRequests: {},
  currentRequest: null,
  requestResponses: null,
  lastIncomingRequestDate: null,
  lastReceivedResponseDate: null,
}

const getters = {
  hasError: state => state.error,
  errors: state => state.errors,
  routes: state => Object.values(state.routes),
  getPersonal: state => Object.values(state.personalRequests),
  getIncoming: state => Object.values(state.incomingRequests),
  currentRequest: state => state.currentRequest,
  successMessage: state => state.successMessage,
  requestResponses: state => state.requestResponses,
  lastIncomingRequest: state => state.lastIncomingRequestDate,
  lastReceivedResponse: state => state.lastReceivedResponseDate,
}

const actions = {
  async ['CREATE_REQUEST']({ commit }, payload) {
    commit(REQUEST_CREATE_BEGIN)
    try {
      const response = await requests.createRequest(payload)
      if (_.isEqual(response.status, 400)) {
        const { data } = response
        commit(REQUEST_CREATE_FAILURE, data)
      }
      commit('SET_SUCCESS_MESSAGE', 'Successfully created an ACMI Request.')
    } catch (errors) {
      commit(REQUEST_CREATE_FAILURE, errors)
    }
  },
  async ['DELETE_REQUEST']({ commit, state }, requestId) {
    try {
      await requests.deleteRequest(requestId)
      const newList = Object.assign({}, ...Object.values(state.personalRequests).map(x => (x.id !== requestId ? {[x.id]: x}: {})))
      commit('SET_PERSONAL_REQUESTS', newList)
      commit('SET_SUCCESS_MESSAGE', 'Successfully deleted')
    } catch (errors) {
      commit('SET_ERROR', errors)
    }
  },
  resetForm({ commit }, route) {
    const newRoutes = {...{[route.id]: route}}
    commit('RESET_FORM', newRoutes)
  },
  ['RESET_SUCCESS_MESSAGE']({ commit }) {
    commit('SET_SUCCESS_MESSAGE', '')
  },
  insertRoute({ commit }, route) {
    const newRoutes = Object.assign({...state.routes}, {[route.id]: route})
    commit(ROUTE_ADD, newRoutes)
  },
  updateRoute({ commit }, route) {
    const newRoutes = Object.assign({...state.routes}, {[route.id]: route})
    commit('ROUTE_UPDATE', newRoutes)
  },
  deleteRoute({ commit }, id) {
    let newRoutes = {...state.routes}
    delete newRoutes[id]
    commit(ROUTE_DELETE, newRoutes)
  },
  ['EMIT_ERRORS']({ commit }, errors) {
    commit(REQUEST_CREATE_FAILURE, errors)
  },
  async [REQUEST_RETRIEVE_PERSONAL]({ commit }) {
    try {
      const { data } = await requests.getPersonal()
      const requestObjects = Object.assign({}, ...data.map(request => ({[request.id]: request})))
      commit('SET_PERSONAL_REQUESTS', requestObjects)
      const lastResponseDate = _.get(_.last(data), 'created_at')
      commit('SET_LAST_RECEIVED_RESPONSE_DATE', lastResponseDate)
    } catch (errors) {
      const { data } = errors
      commit('SET_ERROR', data)
    }
  },
  async ['GET_REQUEST']({ commit }, id) {
    const { data } = await requests.getRequest(id)
    commit('SET_CURRENT_REQUEST', data)
  },
  async ['GET_AVAILABLE_REQUEST']({ commit, state }, id) {
    let request = _.find(state.incomingRequests, {id: id})
    commit('SET_CURRENT_REQUEST', request)
  },
  async ['INITIALIZE_INCOMING_REQUESTS']({ commit }) {
    const { data } = await requests.getAll()
    const requestObjects = Object.assign({}, ...data.map(request => ({[request.id]: request})))
    commit('SET_INCOMING_REQUESTS', requestObjects)
    if (!_.isEmpty(data)) {
      const lastRequest = _.last(data)
      const lastReceivedRequestDate = lastRequest.created_at
      localStorage.setItem('LAST_RECEIVED_REQUEST', lastReceivedRequestDate)
      commit('SET_LAST_INCOMING_REQUEST_DATE', lastReceivedRequestDate)
    }
  },
  async ['POPULATE_RESPONSES']({ commit }, requestId) {
    try {
      const { data } = await requests.getResponses(requestId)
      // let requestResponsesCopy = _.cloneDeep(data)
      // _.forEach(requestResponsesCopy, response => {
      //   response.
      // })

      commit('SET_RESPONSES', data)
    } catch (errors) {
      const { data } = errors
      commit('SET_ERROR', data)
    }
  }
}

const mutations = {
  ['RESET_FORM'](state, routes) {
    state.routes = routes
  },
  ['SET_SUCCESS_MESSAGE'](state, message) {
    state.creating = false
    state.successMessage = message
  },
  ['SET_INCOMING_REQUESTS'](state, payload) {
    state.incomingRequests = payload
  },
  ['SET_LAST_INCOMING_REQUEST_DATE'](state, date) {
    state.lastIncomingRequestDate = date
  },
  ['SET_LAST_RECEIVED_RESPONSE_DATE'](state, date) {
    state.lastReceivedResponseDate = date
  },
  [REQUEST_CREATE_BEGIN](state) {
    state.creating = true
    state.error = false
  },
  [REQUEST_CREATE_FAILURE](state, errors) {
    state.error = true
    state.creating = false
    state.errors = errors
  },
  [ROUTE_ADD](state, routes) {
    state.routes = routes
  },
  ['ROUTE_UPDATE'](state, routes) {
    state.routes = routes
  },
  [ROUTE_DELETE](state, routes) {
    state.routes = routes
  },
  [REQUEST_RETRIEVE_PERSONAL](state, { data }) {
    state.yourRequests = data
  },
  ['SET_PERSONAL_REQUESTS'](state, requests) {
    state.personalRequests = requests
  },
  ['SET_ERROR'](state, errors) {
    state.error = true
    state.errors = errors
  },
  ['SET_CURRENT_REQUEST'](state, request) {
    state.currentRequest = request
  },
  ['SET_RESPONSES'](state, payload) {
    state.requestResponses = payload
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
