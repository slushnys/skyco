import _ from 'lodash'
import core from "../api/core";

const state = {
  contract: null,
  contracts: null,
  creating: false,
  error: false,
  errorDetail: null
}

const getters = {
  // ::::::::  IDEA - GET A SPECIFIC CONTRACT FASTER ::::::::
  // getContract: state => async (id) => {
  //   if (!_.isEmpty(state.contracts)) {
  //     return _.find(state.contracts, {id: id})
  //   }
  //   // just an idea how to get the contracts faster
  //   await dispatch(GET_CONTRACT)
  //   return state.contract
  // },
  getContract: state => state.contract,
  personalContracts: state => state.contracts,
  error: state => state.error,
  getErrorDetail: state => state.errorDetail
}

const actions = {
  async ['GET_CONTRACT']({ commit }, id) {
    const { data } = await core.getContract(id)
    commit('SET_CONTRACT', data)
  },
  async ['GET_CONTRACTS']({ commit }) {
    const { data } = await core.getContracts()
    commit('SET_CONTRACT_LIST', data)
  },
  async ['PROCESS_CONTRACT']({ commit }, payload) {
    try {
      commit('PROCESS_CONTRACT_INITIALIZED')
      if (_.isNil(payload.id)){
        await core.createContract(payload)
      } else {
        await core.updateContract(payload)
      }
      commit('PROCESS_CONTRACT_FINISHED')
    } catch (error) {
      commit('PROCESS_CONTRACT_FAILED', error)
    }
  }
}

const mutations = {
  ['SET_CONTRACT'](state, contract) {
    state.contract = contract
  },
  ['SET_CONTRACT_LIST'](state, contracts) {
    state.contracts = contracts
  },
  ['PROCESS_CONTRACT_INITIALIZED'](state) {
    state.creating = true
  },
  ['PROCESS_CONTRACT_FINISHED'](state) {
    state.creating = false
    state.error = false
  },
  ['PROCESS_CONTRACT_FAILED'](state, error) {
    state.creating = false
    state.error = true
    state.errorDetail = error
  },


}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
