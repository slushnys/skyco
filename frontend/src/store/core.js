import _ from 'lodash'
import auth from '../api/auth'
import core from '../api/core'

const state = {
  user: null,
  company: null,
  globalSettings: {
    settings: null
  },
  stateMessage: null,
  errors: null

}

const getters = {
  getUser: state => state.user,
  company: state => state.company,
  stateMessage: state => state.stateMessage,
  errors: state => state.errors
}

const actions = {
  ['LOAD_USER']({state, commit}) {
    if (state.user) {
      return Promise.resolve(state.user)
    }
    const { data } = auth.getAccountDetails()
    commit('SET_USER', data)
  },
  async ['LOAD_COMPANY']({ commit }) {
    const { data } = await core.getPersonalCompany()
    commit('SET_COMPANY', data)
  },
  async ['COMPANY_UPDATE']({ commit }, payload) {
    let response = null
    if (_.isNil(payload.id)) {
      try {
        response = await core.createCompany(payload)
      } catch (error) {
        commit('SET_ERROR', error)
        throw error
      }
    } else {
      try {
        response = await core.updateCompany(payload)
      } catch (e) {
        throw e
      }
    }
    const { data } = response
    commit('SET_COMPANY', data)
  }
}

const mutations = {
  ['SET_USER'](state, payload) {
    state.user = payload
  },
  ['SET_COMPANY'](state, payload) {
    state.company = payload
  },
  ['SET_ERROR'](state, errors) {
    state.errors = errors
  }
}

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
}
