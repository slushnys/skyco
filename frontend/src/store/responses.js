import responses from '@/api/responses'

const SET_ERROR = 'SET_ERROR'

const state = {
  working: false,
  successMessage: '',
  errors: null,
  error: false,
  currentResponse: null,
  personalResponses: null
}

const getters = {
  hasError: state => state.error,
  errors: state => state.errors,
  currentResponse: state => state.currentResponse,
  successMessage: state => state.successMessage,
  personalResponses: state => state.personalResponses
}

const actions = {
  async ['CREATE_RESPONSE']({ commit }, payload) {
    commit('RESPONSE_CREATE_BEGIN')
    try {
      await responses.createResponse(payload)
      commit('SET_SUCCESS_MESSAGE', 'Successfully created an ACMI Response.')
    } catch (error) {
      commit(SET_ERROR, error)
    }
  },
  async ['MARK_RESPONSE_READ']({ commit }, payload) {
    try {
      const { data } = await responses.update(payload)
      commit('SET_RESPONSE', data)
    } catch (error) {
      console.log(error.response)
      commit(SET_ERROR, error)
    }

  },
  resetForm({ commit }, route) {
    const newRoutes = {...{[route.id]: route}}
    commit('RESET_FORM', newRoutes)
  },
  ['RESET_SUCCESS_MESSAGE']({ commit }) {
    commit('SET_SUCCESS_MESSAGE', '')
  },
  async ['GET_RESPONSE']({ commit }, id) {
    const { data } = await responses.getResponse(id)
    commit('SET_RESPONSE', data)
  },
  async ['GET_PERSONAL']({ commit }) {
    const { data } = await responses.getPersonal()
    commit('SET_PERSONAL', data)
  },
  async ['ACCEPT_RESPONSE']({ commit }, payload) {
    try {
      commit('PROCESS_INITIATE')
      const { data } = await responses.acceptResponse(payload)
      commit('PROCESS_FINISH')
      commit('SET_SUCCESS_MESSAGE', data)
    } catch (errors) {
     const { data } = errors
      commit('SET_ERROR', data)
    }
  }

}

const mutations = {
  ['PROCESS_INITIATE'](state) {
    state.working = true
  },
  ['PROCESS_FINISH'](state) {
    state.working = false
  },
  ['RESET_FORM'](state, routes) {
    state.routes = routes
  },
  ['SET_SUCCESS_MESSAGE'](state, message) {
    state.creating = false
    state.successMessage = message
  },
  ['SET_ERROR'](state, errors) {
    state.working = false
    state.errors = errors
    state.error = true
  },
  ['SET_RESPONSE'](state, response) {
    state.currentResponse = response
  },
  ['SET_PERSONAL'](state, responses) {
    state.personalResponses = responses
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
