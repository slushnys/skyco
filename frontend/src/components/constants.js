import _ from 'lodash'

const RouteType = {
  FERRY: 'ferry',
  LIVE: 'live'
}

const arrivalDate = (routes) => {
  if (routes.length > 0) {
    return _.get(routes[0], 'arrival_at')
  }
  return null
}
const isFerry = (routes) => {
  if (routes.length > 0) {
    return _.get(routes[0], 'type') === RouteType.FERRY ? 'Yes' : 'No'
  }
  return null
}

export {
  RouteType,
  arrivalDate,
  isFerry
}
