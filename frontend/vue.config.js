const path = require('path')

module.exports = {
  assetsDir: 'static',
  chainWebpack: config => {
    config.resolve.alias.set('@',  path.join(__dirname, 'src'))
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:8000',
        ws: true,
        changeOrigin: true
      },
      '/auth': {
        target: 'http://localhost:8000',
      },
      '/registration': {
        target: 'http://localhost:8000',
      }
    }
  }
}
