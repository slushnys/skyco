Ideas that came from GetJet company as a leaser.

//REQUESTS
* When creating request have some fields for additional conditions, e.g. fueling and handling in certain cities, countries.
//CONTRACTS
* When creating a contract, add an option to upload the contract itself. Maybe at some point parse that data as well.
* Extend agreed contract, flight and route. e.g. Company A requests an aircraft and accepts company's B response for certain day of 2018-06-30, because they're waiting for a plane part, however the part is late for one day, so they need the aircraft for 2018-06-30 as well. **Implement posibility to change, extend already agreed contract and its routes.** 

* Implement ferry flight options within the response of a request. Add number of crew members.
* Implement ferry flight automation calculation logic. Every new day of request accounts to multiplier of the Per diem price and the amount of crew members.

* Different price for ferry flights. (part of negotiation maybe)
* Negotiations in response prices
* Have a physical signature that companies could sign contracts with. Solutions: (docusign? AAS) or provide different font (handwritting font)
* RAIDO - integrate scheduling system that the companies are using to allow them to see when they have planes available,
* Make a checklist to know what companies require when requesting/ trying to respond to the request. (what do companies need, want?)
* whitelist
* Add additinoal conditions to request creation.
* Option to upload their custom contract straight from computer (while editin on the go)?

* How most optional request - response workflow could be achieved:
    1. Company creates a request.
    2. Leasing aircraft company reviews it, they like the timing and routes.
    3. They send the requested routes to their OCC, financial, and crew departments through an email.
    4. OCC and other departments review the routes through the email and if they agree with them, they just click accept button,
    which already has a link automatically generated for approving the request times.
    5. The ACMI department then can proceed with issuing the response to the requesting company with times provided by OCC department.
*                 