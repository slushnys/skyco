# TODO

1. (done) Add a static file for bootstrap styles. Not only that, but all the other static files required for this project I could work on the styles also offline.
2. (done) Add moment.js to the frontend
3. Don't allow to use others contracts to create responses
4. (done) Let user edit his company details.
5. Let user make a response
6. (done) Let user create and manage contracts.
7. Make all the date times ZULU. All times should be UTC
8. !DO NOT! Let users create request or response until they have a company created (fill in the company settings)
9. Create default popups when something has been successfully completed as well as display failed attemps in there.
10. If a user edited the response, the user who needs to accept the responds should not be able to do so.
11. CONVERT EVERYTHING TO USE UTC
12. If a response to a request has been accepted, other users should not be able to create requests anymore.
13. Define the rule for when a request time should end and users should not be able to respond to it anymore.
14. !MUST! create a tipe refresh of core state (pull from database) to check for:
    new requests
    new responses
15. Add VueToasted for notification messages.

To create notifications of new responses to ones request we need a polling system.
It would check every 10 seconds if your current responses number is equal to the one received from the summary api.
If the number retrieved from the API is greater, then we add the difference to TheSideBar component.
Once user checks his responses, the list of responses updates and the counter of new responses is set back to 0.

Similar logic should be done with chat messages. For this particular case, last read message timestamp should be taken into account.
If user read a message 14:33 the last time and 5 new chat messages appears in the database at 14:36 the count of total messages is taken into account
from that time and is displayed for user to view.

#BUGS
* When first time logged in, user and information of user is not registered in the store.

## USE BEST PRACTICES OF VUE REPRESENTED IN OFFICIAL RULES
* Fix contract detail page. when user opens it, page makes a GET request to backend with id of 'create' which responds with 404
* Use CamelCase file names.
* Use TheFile (The) in the beginning of the file name if the component is used once per page. (Single-instance component names)
* Avoid v-if together with v-for
* Use style scoping for components and use them everywhere, have as much customized class names as possible.
* Order of words in component names rule. Start with generic names that are contextual to your app. Have all the components under 'components' folder, will be easier to navigate.
* 


# User stories:

## Person visits a page
  1. Watch an introduction video
  2. Read about how the system works
  3. Contact us
  4. Sign in
      1. Click on signup from any screen in our landing page.
      2. Opens a signup form consisting of:
          1. First and Last names
          2. Email (only from air company domains are allowed)
          3. Password
          4. Air company name
      3. Click on Finish (finalise signup)
  5. Sign up



## Sign in process

1. Click on Sign In from anywhere in landing page
2. Get a small Modal, popup, dropdown with content:
    1. Email input
    2. Password input
3. Click on “Enter” button to enter the panel

## Confirmation for the registration
1. They receive an email to confirm their registration
2. Email contains details of what they have to provide to confirm their account

## Creating a contract

User goes to the settings page, and clicks on Contracts page.
There he can create a new conract which he will be able to use to send to other companies.

He clicks a button and is redirected to "Create contract" page where he has to fill in the data.

### Data to be filled
```json
{
  "name": "Test contract",
  "additonal_conditions": "large amount of text.",
}
```

After filling out the data


## Making a ACMI Request 
Enter data and create a request from it

## Making a response to ACMI Request

User making a response can view the routes of the request, however he cannot change them.
In order to make a request response details have to be filled - e.g. price per block hour, min block hours, etc.

In addition to that, a contract has to be chosen with which the requesting user will have to agree.
The contract should be automatically generated and prefilled with the response data. A template contract is provided with all basic details already in it, users however can extend the contract themselves inside the settings page of the application.

The  "deal" then is made based on the contract. A contract confirms the deal, even though more documents can be attached to the response. In that sense if a request has a response with flag `accepted` then it means the requesting user has accepted the response and is in contract with that company providing them with the aircraft.




  PANEL:
    """
    There is no renting/requesting option in our system, you receive requests from other companies and you can request other companies )
    """

    ### Newcomers (just registered)###
          Newcomers should register their planes in the system with the posibility to select the passenger number

          Screen should contain of a menu on the right side and a main context in the rest of the screen. (maybe breadcrumbs at the top).
          First window, the new comers would see after logging in, is a tour screen (in a way). Then the logo appears in the middle
      to add the planes that you want to rent - you are asked to add the planes you have for ACMI requests (airplanes)

      * It’s not mandatory but nice to have, they then will receive the requests as well, otherwise they just be able to send requests to others.
          (request has to include the plane type which the company would be looking for or is it
                      OR
      * They can add a plane they are looking for within the search when they are about to sent request for one. (in this case
          there should be nice ui/ux for this)

    ### SCREEN AVAILABILITY ###
        _________________________________
        |         |                                     |       This screen estate would allow same time management
        |         |                                     |         of requests that have been sent and requests that arrive for certain types.
        |         |             YOUR REQUESTS        |       For MVP we would need to provide:
        |         |                                     |         * Real time tracking what's happening with requests for ACMI.
        |         |                                     |         * Create (and customize) a contract for when a company is replying
        |         |------------------------ |             to request saying that they will issue the plane.
        |         |                                     |            (Wen a company agrees to issue the plane, they have a popup, or just some screen space
        |         |                                     |               to fill in the price for time that the plane is requested.)
        |         |    OTHERS REQUESTING YOURS    |             When a price to other company is sent, together a custom contract is generated and sent together.
        |         |                                     |             When a requesting company receives the office with price and contract it can agree to the contract
        |         |                                     |               and by that signing it electronically (as it should be mentioned in terms and conditions)
        ---------------------------------


# TERMS AND CONDITIONS
1. When making a plane + crew offer you automatically sign the contract on your determined (custom) conditions that were used to create that contract.

2. When receiving a offer with a price and a contract, by agreeing to that contact you sign that contract electronically. This also means that you will pay for that price received.


# RESEARCH REQUIRED
1. How do contracts between airlines look like when they're about to lease, or get a leased plane? From this we need to create contracts that would be signed by other companies.



  At the end product what could be possible is:
  > When someone responds with a changed routes that some might be missing and that person who requested flights accepts - system could offer another request creation automatically based on those times and those locations so the user would be able to save his time.
  

TODO:
  1. Create a request with: (v)
    * Date, Time, Call sign, passenger number, from -> to
  2. view your requests (V)
  3. Create a response to others request. (v)
  4. Check your responses ()
  5. Check who responded to your request and what kind of propositions have they made 
  6. Accept a request.
  7. Allow both parties to review the contract
  8. Enable chat for both parties with each other
  9. Finalize the presentation screen
    


# API Information

All the endpoints will begin with /api/v{version_number} which will not be included in the upcoming explanation.
Endpoints described here will be used in the frontend.

## ACMI Requests

GET /requests - Returns all the requests that have been created (excluding the users which is looking for requests)
POST /requests - Creates a new request out of data provided.

GET /requests/{pk}/ - Retrieve request details.
  - If the request is users' who has created it, then he should have the rights to edit it.
  - If lookin at other company request, you can create a response to it having details of request.

GET /requests/{pk}/responses - Returns all responses from other companies that have created a response for  this request on this particular ID.
GET /requests/{pk}/responses/5 - Not working. Should it?

DELETE /requests/{pk}/ - Deletes specified request. Keep in mind that if there has been an accepted response, a request cannot be easily deleted.

GET /your_requests - Returns all requests that have been created by the user logged into the system

## ACMI Responses 

### Get all responses
[DONE] GET /responses/ - Gets all responses that a user has made (youself).

### Get certain response 
<font color="green">**GET**</font> /responses/{pk}/ - gets information from a certain response based on a pk
### Create a response
><font color="orange">**POST**</font> /responses/ - Create a response for a particular request. Passing along the data including acmi_request: pk

### Accept a response

><font color="orange">**POST**</font> /responses/{id}/accept

A POST request endpoint sending a post request for an existing response (which is not your own) should accept the response for a certain request.
data passed along: {
  "acmi_request": "1"
}


### Delete a response

><font color="red">**DELETE**</font> /responses/{id}/

Deletes a response. Only allowed for the user who created the response to execute


#### Logical flow

When creating a response a user is able to view request information with its routes.
Use can edit the data specific data of those routes, e.g.: date, time and add a ferry flights.
Ferry flights are additional routes.

save routes according to the 

pass a dictionary like the following:
{
  "acmi_request": 1,
  "min_block_hour" 5,
  ...
}

> Validating the routes in response compared to request routes

Routes in request are ordered from the newest to the oldest.

For each route of response, it should be checked if it doesn't exceed the required time.
(does it need that tho? Maybe air company has a treshold of being late.)



# Testing

## Operations


### User should be able to view others requests when authenticated
Done
### User should be able to create response to others requests
### User should be able to create response to others requests with ferry flights
### User should be able to view his own requests
### User should be able to view his own responses
### User should be able to create a contract
### User should be able to edit his own requests
### User should be able to delete his own requests
### User should be able to delete his own responses only if the response hasnt been accepted.
### User should be able to delete his contracts


### User should not be able to view requests if not authenticated 
Done
### User should not be able to respond to its own requests

### User should not be able to edit any response (including his own)
### User should not be able to view others responses
### User should not be able to view others contracts
### User should not be able to edit his own requests.


## Users
### Can register
Done
### Can login
Done






# Talk for the presentation

## Building credibility 
Hello everyone who gathered here and spared some time to hear us out.

My name is Zigmas Slusnys and this is partner of mine - Edhgaras Lukasevicius.

I have been been building my experience outside of Lithuania since years ago. when I travelled to United Kindgdom to starting my Bachelors of Software Engineering course in of of the universities in Netherlands.

During those years I was working with wholesale company in a field of ecommerce that was flipping huge amounts of products to other smaller shops. They had a problem of uploading multiple products to sell to multiple shops at the same time and at the time that process took a person a total of one hour. I have successfully decreased that process to 5 minutes which is 20x faster. Even though this project was exciting I wanted to try a job in a company that would represent the world of enterprise. Therefore I did. I went to work to company called CGI which is one of the biggest software consultancy companies and I had the honor to be a business data analyst working closely with two of largest banks in Netherlands to make a software that would analyze processes - that was called process mining. With the solution I created after analyzing banking processes I was able identify bottlenecks that were stacking up and reducing the best case scenario workflow by days.

## 
I wanted to ask all of you here a mutual question to your profession when it comes to air operations and aircraft wet leasing.

Before we begin the presentation I wanted to ask everyone couple of questions:
* How much time on average does it take you to find a wet lease when an aircraft malfunctions?
* Are you strugling with administration when it comes to processing contracts and payments?
* How often do you feel that the brokers are not trustworthy to handle the negotiations or to find a wet lease for you?


Questions: 
* How much nerve and stress does it cost you to find a wet lease on time and to have a decent communication with the broker?
* Would you like direct communication between the air operation companies just like you?
* How much would you appreciate knowing who you’re talking to without any secrets and full transparency?
