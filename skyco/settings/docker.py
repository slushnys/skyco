from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'skyco_panel',
        'USER': 'skyco_admin',
        'PASSWORD': 's0m3r4nd0mstr1ng',
        'HOST': 'db',
        'PORT': 5432,
    }
}
