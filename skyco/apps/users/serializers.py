from rest_framework import serializers
from skyco.apps.users.models import Company, User


class CompanySerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Company
        fields = '__all__'
        # exclude = ('user',)


class UserSerializer(serializers.ModelSerializer):
    company = CompanySerializer()

    class Meta:
        model = User
        fields = (
            'email',
            'company',
            'is_active',
            'first_name',
            'last_name'
        )
