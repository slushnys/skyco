from django.contrib.auth.forms import UserCreationForm
from skyco.apps.users.models import User


class SignUpForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('email', 'password1', )
