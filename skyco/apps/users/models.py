import uuid

from django.db import models
from django.contrib.auth.models import AbstractUser


class Company(models.Model):
    name = models.CharField(max_length=40)
    registration_number = models.CharField(max_length=20)
    telephone = models.CharField(max_length=20)
    street = models.CharField(max_length=60)
    post_code = models.CharField(max_length=100)
    country = models.CharField(max_length=60)
    email = models.CharField(max_length=60)
    user = models.OneToOneField('User', on_delete=models.DO_NOTHING, null=False)


class User(AbstractUser):
    user_id = models.AutoField(primary_key=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['password', ]
