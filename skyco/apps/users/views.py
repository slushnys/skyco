from django.contrib.auth import authenticate, login
from django.shortcuts import redirect, render
from rest_framework.response import Response
from rest_framework import permissions, viewsets, mixins
from rest_framework.decorators import action

from skyco.apps.users.models import Company, User
from skyco.apps.users.forms import SignUpForm
from skyco.apps.users.serializers import CompanySerializer, UserSerializer


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'auth/signup.html', {'form': form})


class CompanyViewSet(viewsets.ModelViewSet):

    model = Company
    queryset = model.objects.none()
    serializer_class = CompanySerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return Company.objects.filter(user=self.request.user.pk)

    def update(self, request, *args, **kwargs):
        import ipdb; ipdb.set_trace()
        return super().update(request, *args, **kwargs)

    @action(methods=['get'], detail=False, url_name='me', url_path='me')
    def me(self, request, *args, **kwargs):
        company_serializer = self.serializer_class(self.get_queryset().first())
        return Response(company_serializer.data)


class UserViewSet(viewsets.ReadOnlyModelViewSet, mixins.UpdateModelMixin):
    model = User
    serializer_class = UserSerializer
    queryset = model.objects.none()
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return self.model.objects.get(pk=self.request.user.pk)

    @action(methods=['get'], detail=False, url_name='me', url_path='me')
    def me(self, request, *args, **kwargs):
        user_serializer = self.serializer_class(self.get_queryset())
        return Response(user_serializer.data)
