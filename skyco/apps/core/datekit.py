import datetime
from django.utils import timezone


def datetime_to_epoch(dateobj):
    """
    Converts a datetime object to JS epoch representation.
    """
    if dateobj:
        if not timezone.is_aware(dateobj):
            dateobj = timezone.make_aware(dateobj)
        return int(dateobj.timestamp() * 1000)


def epoch_to_datetime(timestamp, country_tz=timezone.utc):
    """
    Converts an epoch timestamp to a valid python timestamp.
    """
    return timezone.make_aware(
        datetime.datetime.utcfromtimestamp(
            int(timestamp) / 1000), country_tz)
