import json
from django.test.client import Client
from skyco.apps.users.models import User


class DummyUserFactory:
    def create_user():
        pass


class RequestTestClient(Client):

    """
    Wraps attributes for json to the django test client.
    """

    def json_support(func):
        def wrapped(self, *args, **kwargs):
            if 'json' in kwargs:
                kwargs['data'] = json.dumps(kwargs['json'])
                kwargs['content_type'] = 'application/json'
            return func(self, *args, **kwargs)
        return wrapped

    @json_support
    def post(self, *args, **kwargs):
        super().post(*args, **kwargs)
