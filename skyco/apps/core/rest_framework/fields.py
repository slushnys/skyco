from rest_framework import fields
from skyco.apps.core import datekit


class EpochJSDateField(fields.DateTimeField):
    """
    We're intentionally not relying on DRF to render the datetime into a
    timezone based string. If we do that, we would need a proper datetime
    parser on the JS side, pulling in dependencies like Moment.js whereas epoch
    parsing is simply a manner of ``new Date(123)``. Let's KISS.
    """

    def to_representation(self, value):
        return datekit.datetime_to_epoch(value)

    def to_internal_value(self, value):
        return datekit.epoch_to_datetime(value)
