from django.views import generic
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from rest_framework import status
from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from skyco.apps.core.rest_framework.fields import EpochJSDateField


from skyco.apps.operations.exceptions import RequestHasAcceptedResponse
from skyco.apps.operations.models import ACMIRequest, ACMIResponse
from skyco.apps.operations.serializers import ACMIRequestSerializer, ACMIResponseSerializer


class ACMIRequestListView(generic.ListView):
    model = ACMIRequest
    template_name = 'operations/requests_list.html'
    context_object_name = 'recent_requests'

    def get_queryset(self):
        """Return the last five published questions."""
        return ACMIRequest.objects.all()


class ACMIRequestCreateView(CreateView):
    model = ACMIRequest
    fields = []

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class ACMIRequestDetailView(DetailView):
    model = ACMIRequest
    template = 'operations/acmirequest_detail.html'


# def index(request):
#     recent_requests = ACMIRequest.objects.filter(active=True)
#     context = {
#         'recent_requests': recent_requests
#     }
#     return render(request, 'acmi_request_list.html', context)


class ACMIRequestAPI(ModelViewSet):
    model = ACMIRequest
    queryset = ACMIRequest.objects.all()
    serializer_class = ACMIRequestSerializer
    permission_classes = (IsAuthenticated,)

    # TODO: Allow users to view requests only that are not theirs. /
    # Personal requests are different.

    def list(self, request, *args, **kwargs):
        queryset = ACMIRequest.objects.exclude(user=self.request.user)
        return Response(data=self.serializer_class(queryset, many=True).data)

    @action(methods=['get'], detail=False, url_path='personal', url_name='personal')
    def personal(self, request):
        queryset = self.queryset.filter(user=self.request.user, deleted=False)
        serializer = self.serializer_class(queryset, many=True)
        return Response(data=serializer.data, content_type='json')

    @action(methods=['get'], detail=True, url_path='responses', url_name='requests-responses')
    def responses(self, request, pk):
        acmi_request = self.get_object()
        if acmi_request.user != request.user:
            raise ValidationError('You can only see responses to your own requests.')
        serializer = ACMIResponseSerializer(acmi_request.responses.all(), many=True)
        return Response(data=serializer.data)

    @action(methods=['post'], detail=True, url_path='accept', url_name='requests-accept-response')
    def accept(self, request, pk):
        acmi_request = self.get_object()
        if acmi_request.user != request.user:
            raise ValidationError('You can not accept responses to your own requests.')
        acmi_response = request.data.get('acmi_response', None)
        if not acmi_response:
            raise ValidationError('ACMI Response id is missing.')
        acmi_response = ACMIResponse.objects.get(pk=acmi_response)
        acmi_request.accept_response(acmi_response)
        return Response(status=status.HTTP_200_OK)

    def perform_destroy(self, instance):
        try:
            instance.mark_deleted()
        except RequestHasAcceptedResponse as e:
            raise ValidationError(e.message)


class ACMIResponseAPI(ModelViewSet):
    model = ACMIResponse
    queryset = ACMIResponse.objects.all()
    serializer_class = ACMIResponseSerializer
    permission_classes = (IsAuthenticated,)

    def perform_delete(self, instance):
        instance.mark_deleted()

    @action(methods=['get'], detail=False, url_path='personal', url_name='personal')
    def personal(self, request):
        queryset = self.queryset.filter(user=self.request.user)
        serializer = self.serializer_class(queryset, many=True)
        return Response(data=serializer.data, content_type='json')

    @action(methods=['post'], detail=True, url_path='accept', url_name='response-accept')
    def accept(self, request, pk):
        acmi_response = self.get_object()
        if acmi_response.user == request.user:
            raise ValidationError('You can not accept responses to your own requests.')
        acmi_request = request.data.get('acmi_request', None)
        if not acmi_request:
            raise ValidationError('ACMI Request id is missing.')

        acmi_request = ACMIRequest.objects.get(pk=acmi_request)
        if acmi_request.responses.filter(accepted=True):
            raise ValidationError('You have already accepted a response to this request.')
        acmi_request.accept_response(acmi_response)
        return Response(data={'message': 'You have successfully accepted the response'}, status=status.HTTP_200_OK)

    @action(methods=['post'], detail=True, url_path='update', url_name='response-update')
    def update_response(self, request, pk):
        acmi_response = self.get_object()

        viewed_at = request.data.get('viewed_at')
        previous_response_count = request.data.get('previous_response_count')

        fields_to_update = []

        if viewed_at:
            acmi_response.viewed_at = EpochJSDateField().to_internal_value(viewed_at)
            fields_to_update.append('viewed_at')

        if previous_response_count:
            acmi_response.previous_response_count = previous_response_count
            fields_to_update.append('previous_response_count')

        if fields_to_update:
            acmi_response.save(update_fields=fields_to_update)
            serializer = self.serializer_class(acmi_response)
            return Response(data=serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class SummaryAPIViewset(ViewSet):
    permission_classes = (IsAuthenticated,)

    def list(self, request):
        epoch_converter = EpochJSDateField()
        responses_counter = self.get_personal_request_responses_count(request)
        latest_request_date = epoch_converter.to_representation(self.get_latest_request_date(request))
        latest_response_date = epoch_converter.to_representation(self.get_latest_response_date(request))
        # TODO: Implement latest message for this user method property.
        # latest_message_date = self.get_latest_message_date(request)
        return Response(data={
            'responses_counter': responses_counter,
            'latest_request_date': latest_request_date,
            'latest_response_date': latest_response_date,
        })

    def get_latest_request_date(self, request):
        newest_request = ACMIRequest.objects.exclude(user=request.user)
        if not newest_request.count():
            return None
        return newest_request.last().created_at

    def get_personal_request_responses_count(self, request):
        acmi_requests = ACMIRequest.objects.personal(user=request.user).filter(active=True)
        responses_counter = 0
        for personal_request in acmi_requests:
            responses_counter += personal_request.responses.count()
        return responses_counter

    def get_latest_response_date(self, request):
        responses_to_personal_requests = ACMIResponse.objects.to_personal_requests(user=request.user)
        if not responses_to_personal_requests.count():
            return None
        return responses_to_personal_requests.last().created_at

    # TODO: Implement latest messages date
    # def get_latest_message_date(self, request):
    #     messages = Messages.objects.filter(user=request.user)
    #     if not messages:
    #         return None
    #     return messages.last().created_at
