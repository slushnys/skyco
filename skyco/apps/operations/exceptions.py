
class RequestHasAcceptedResponse(Exception):
    message = 'You cannot delete this request that you already accepted a response to.'


class ResponseAcceptedException(Exception):
    message = 'Accepted responses can not be deleted.'
