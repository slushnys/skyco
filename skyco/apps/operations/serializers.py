import math
from decimal import Decimal
from django.db import transaction
from django.conf import settings
from rest_framework.exceptions import ValidationError

from rest_framework import serializers

from skyco.apps.core.rest_framework.fields import EpochJSDateField
from skyco.apps.operations.models import ACMIRequest, ACMIResponse, Contract, Route
from skyco.apps.users.models import Company


# class AirportSerializer(serializers.PrimaryKeyRelatedField):
#     queryset = Airport.objects.all()

#     def to_internal_value(self, data):
#         if isinstance(data, int):
#             return Airport.objects.get(pk=data)
#         elif isinstance(data, Airport):
#             return data
#         return self.fail('Does not exist', pk_value=data)

#     def to_representation(self, value):
#         airport = Airport.objects.get(pk=value.pk)
#         return {
#             'id': airport.pk,
#             'code': airport.code,
#         }

class ContractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contract
        exclude = ('user', 'created_at', 'updated_at')


class RouteSerializer(serializers.ModelSerializer):
    departure_at = EpochJSDateField()
    arrival_at = EpochJSDateField()
    type = serializers.CharField()
    created_at = EpochJSDateField(read_only=True)
    updated_at = EpochJSDateField(read_only=True)

    class Meta:
        model = Route
        fields = (
            'departure',
            'arrival',
            'departure_at',
            'arrival_at',
            'passenger_number',
            'type',

            'created_at',
            'updated_at',
        )


class ACMIRequestSerializer(serializers.ModelSerializer):
    # plane = PlaneSerializer()
    id = serializers.IntegerField(source='pk', required=False)
    routes = RouteSerializer(many=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    created_at = EpochJSDateField(read_only=True)
    updated_at = EpochJSDateField(read_only=True)
    responses = serializers.SerializerMethodField()
    latest_response_date = serializers.SerializerMethodField()

    class Meta:
        model = ACMIRequest
        fields = (
            'id',
            'routes',
            # Only until User validation will be instantiated. Then user will be taken from request.user
            'user',
            'created_at',
            'updated_at',
            'responses',
            'latest_response_date'
        )

    def get_responses(self, obj):
        return obj.responses.values_list(flat=True)

    def get_latest_response_date(self, obj):
        if not obj.responses.count():
            return None
        return EpochJSDateField().to_representation(obj.responses.last().created_at)

    def validate(self, data):
        user = data['user']
        if not hasattr(user, 'company'):
            raise serializers.ValidationError('You need to configure your company information first.')
        return super().validate(data)

    @transaction.atomic
    def create(self, validated_data):
        routes_data = validated_data.pop('routes')
        instance = super().create(validated_data)
        # sets each route to have a parent of newly created
        if not routes_data:
            raise serializers.ValidationError('Routes have to be specified.')
        for route in routes_data:
            Route.objects.create(acmi_request=instance, **route)
        return instance


class ACMIResponseSerializer(serializers.ModelSerializer):
    routes = RouteSerializer(many=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    company = serializers.SerializerMethodField()
    created_at = EpochJSDateField(read_only=True)
    updated_at = EpochJSDateField(read_only=True)
    contract = ContractSerializer()
    viewed_at = EpochJSDateField()

    pricing_summary = serializers.SerializerMethodField()

    class Meta:
        model = ACMIResponse
        fields = (
            'id',
            'user',
            'accepted',
            'price_block_hour',
            'daily_allowance',
            'min_block_hour',
            'plane',
            'early_payment',
            'base',
            'acmi_request',
            'routes',
            'contract',

            'company',

            'created_at',
            'updated_at',
            'viewed_at',

            'pricing_summary'
        )

    def get_company(self, obj):
        try:
            return {'name': obj.user.company.name, 'logo': ''}
        except Company.DoesNotExist:
            return ''

    def validate(self, data):
        if isinstance(data['acmi_request'], ACMIRequest):
            acmi_request = data['acmi_request']
        else:
            acmi_request = ACMIRequest.objects.get(pk=data['acmi_request'])
        if data['user'] == acmi_request.user:
            raise ValidationError('You can not create a response to your own request.')
        try:
            data['user'].company
        except Company.DoesNotExist:
            raise ValidationError({'company': 'You need to configure your company information first.'})
        return super().validate(data)

    def create(self, validated_data):
        routes_data = validated_data.pop('routes')
        instance = super().create(validated_data)
        if not routes_data:
            raise serializers.ValidationError({'routes': 'Routes have to be specified.'})

        for route in routes_data:
            Route.objects.create(acmi_response=instance, **route)

        return instance

    def get_pricing_summary(self, obj):
        if not obj.routes.count():
            return {}
        first_route = obj.routes.first()
        last_route = obj.routes.last()

        total_time_span = last_route.arrival_at - first_route.departure_at
        total_hours = math.ceil(total_time_span.total_seconds() / (60 * 60))
        total_days = total_time_span.days + int(total_time_span.seconds > 1)

        total_daily_allowance = obj.daily_allowance * total_days
        # TODO: Add handling fees into the sum
        price_sum = (obj.price_block_hour * total_hours) + total_daily_allowance
        commision = price_sum * settings.SKYCO_COMMISION
        final_price = price_sum + commision

        return {
            'estimated_hours': total_hours,
            'estimated_days': total_days,
            'total_daily_allowance': total_daily_allowance,
            'price_sum': Decimal(price_sum),
            'total_price': Decimal(final_price),
            'commission': Decimal(round(commision))
        }
