import pytest
from skyco.apps.operations.models import ACMIRequest, Route, ACMIResponse
from django.utils import timezone
from datetime import timedelta


@pytest.fixture
def john_acmi_request(user_john):
    acmi_request = ACMIRequest.objects.create(
        plane='A320',
        user=user_john,
    )
    Route.objects.create(
        departure='VNO',
        arrival='EHV',
        departure_at=timezone.now() + timedelta(hours=3),
        arrival_at=timezone.now() + timedelta(hours=5),
        passenger_number=60,
        call_sign='AX984RR',
        type='live',
        acmi_request=acmi_request,
    )
    return acmi_request


@pytest.fixture
def marie_acmi_request(user_marie):
    acmi_request = ACMIRequest.objects.create(
        plane='A320',
        user=user_marie,
    )
    Route.objects.bulk_create([
        Route(
            departure='AMS',
            arrival='EHV',
            departure_at=timezone.now() + timedelta(hours=1),
            arrival_at=timezone.now() + timedelta(hours=3),
            passenger_number=80,
            call_sign='ABCALL123',
            type='live',
            acmi_request=acmi_request,
        ),
        Route(
            departure='EHV',
            arrival='VNO',
            departure_at=timezone.now() + timedelta(hours=5),
            arrival_at=timezone.now() + timedelta(hours=8),
            passenger_number=100,
            call_sign='ABCALL123',
            type='live',
            acmi_request=acmi_request
        )
    ])
    return acmi_request


@pytest.fixture
def marie_request_with_accepted_johns_response(marie_acmi_request, user_john, john_contract):
    acmi_response = ACMIResponse.objects.create(
        plane='Boening 737',
        user=user_john,
        accepted=True,
        price_block_hour=2500,
        daily_allowance=77,
        min_block_hour=5,
        early_payment=True,
        base='AMS',
        acmi_request=marie_acmi_request,
        contract=john_contract
    )
    Route.objects.bulk_create([
        Route(
            departure='AMS',
            arrival='EHV',
            departure_at=timezone.now() + timedelta(hours=1),
            arrival_at=timezone.now() + timedelta(hours=3),
            passenger_number=80,
            call_sign='ABCALL123',
            type='live',
            acmi_response=acmi_response,
        ),
        Route(
            departure='EHV',
            arrival='VNO',
            departure_at=timezone.now() + timedelta(hours=5),
            arrival_at=timezone.now() + timedelta(hours=8),
            passenger_number=100,
            call_sign='ABCALL123',
            type='live',
            acmi_response=acmi_response
        )
    ])
    return marie_acmi_request
