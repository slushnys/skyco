import json
from django.urls import reverse
from skyco.apps.operations.models import ACMIRequest, ACMIResponse, RouteType, Route
from django.utils import timezone
from skyco.apps.core.datekit import datetime_to_epoch


def response_payload(user, acmi_request, plane_name, early_payment, contract, routes):
    return json.dumps({
        "user": user.pk,
        "acmi_request": acmi_request.pk,
        "price_block_hour": 2800,
        "daily_allowance": 80,
        "min_block_hour": 5,
        "plane": plane_name,
        "early_payment": early_payment,
        "base": "EHV",
        "contract": contract.pk,
        "routes": routes
    })


def test_get_requests_not_authenticated(client):
    response = client.get(reverse('acmirequest-list'))
    assert response.status_code == 403


def test_get_requests_endpoint_authenticated(john_authenticated, client):
    response = client.get(reverse('acmirequest-list'))
    assert response.status_code == 200


def test_user_john_can_see_others_requests(
    user_john, john_authenticated, client, marie_acmi_request
):
    response = client.get(reverse('acmirequest-list'))
    assert len(response.data) == 1
    assert ACMIRequest.objects.count() == 1


def test_user_john_should_not_see_personal_requests(
    user_john, john_authenticated, client, john_acmi_request
):
    response = client.get(reverse('acmirequest-list'))
    assert len(response.data) == 0
    assert ACMIRequest.objects.count() == 1


# ACMI Response
def test_user_should_be_able_to_create_response_to_others_requests(
    user_john, john_authenticated, client, marie_acmi_request, john_contract
):
    routes = [
        {
            "departure": "AMS",
            "arrival": "VNO",
            "departure_at": datetime_to_epoch(timezone.now()),
            "arrival_at": datetime_to_epoch(timezone.now()),
            "passenger_number": 180,
            "type": RouteType.LIVE
        },
        {
            "departure": "VNO",
            "arrival": "AMS",
            "departure_at": datetime_to_epoch(timezone.now()),
            "arrival_at": datetime_to_epoch(timezone.now()),
            "passenger_number": 180,
            "type": RouteType.LIVE
        }
    ]
    acmi_response = response_payload(
        user_john, marie_acmi_request, 'Plane 320', early_payment=True, contract=john_contract, routes=routes)
    response = client.post(reverse('acmiresponse-list'), data=acmi_response, content_type='application/json')
    assert response.status_code == 201
    ACMIResponse.objects.count() == 1


# ACMI Responses category
def test_user_should_be_able_to_create_response_to_others_requests_with_ferry_flights(
    user_john, john_authenticated, client, marie_acmi_request, john_contract
):
    routes = [
        {
            "departure": "LDN",
            "arrival": "AMS",
            "departure_at": datetime_to_epoch(timezone.now()),
            "arrival_at": datetime_to_epoch(timezone.now()),
            "passenger_number": 180,
            "type": RouteType.FERRY
        },
        {
            "departure": "AMS",
            "arrival": "VNO",
            "departure_at": datetime_to_epoch(timezone.now()),
            "arrival_at": datetime_to_epoch(timezone.now()),
            "passenger_number": 180,
            "type": RouteType.LIVE
        },
        {
            "departure": "VNO",
            "arrival": "AMS",
            "departure_at": datetime_to_epoch(timezone.now()),
            "arrival_at": datetime_to_epoch(timezone.now()),
            "passenger_number": 180,
            "type": RouteType.LIVE
        },
        {
            "departure": "AMS",
            "arrival": "LDN",
            "departure_at": datetime_to_epoch(timezone.now()),
            "arrival_at": datetime_to_epoch(timezone.now()),
            "passenger_number": 180,
            "type": RouteType.FERRY,
        },
    ]
    acmi_response = response_payload(
        user_john, marie_acmi_request, 'Boening 767', early_payment=True, contract=john_contract, routes=routes)
    response = client.post(reverse('acmiresponse-list'), data=acmi_response, content_type='application/json')
    assert response.status_code == 201
    ACMIResponse.objects.count() == 1
    acmi_response_instance = ACMIResponse.objects.last()

    Route.objects.filter(acmi_response=acmi_response_instance, type=RouteType.FERRY).count() == 2


def test_user_should_be_able_to_view_his_own_requests(
    client, user_john, john_acmi_request
):
    response = client.get(reverse('acmirequest-personal'))
    assert len(response.data) == 1


def test_user_should_be_able_to_view_his_own_responses(
    user_john, john_authenticated, client, marie_acmi_request, john_contract,
):
    routes = [
        {
            "departure": "LDN",
            "arrival": "AMS",
            "departure_at": datetime_to_epoch(timezone.now()),
            "arrival_at": datetime_to_epoch(timezone.now()),
            "passenger_number": 180,
            "type": RouteType.FERRY
        },
        {
            "departure": "AMS",
            "arrival": "VNO",
            "departure_at": datetime_to_epoch(timezone.now()),
            "arrival_at": datetime_to_epoch(timezone.now()),
            "passenger_number": 180,
            "type": RouteType.LIVE
        },
        {
            "departure": "VNO",
            "arrival": "AMS",
            "departure_at": datetime_to_epoch(timezone.now()),
            "arrival_at": datetime_to_epoch(timezone.now()),
            "passenger_number": 180,
            "type": RouteType.LIVE
        },
        {
            "departure": "AMS",
            "arrival": "LDN",
            "departure_at": datetime_to_epoch(timezone.now()),
            "arrival_at": datetime_to_epoch(timezone.now()),
            "passenger_number": 180,
            "type": RouteType.FERRY,
        },
    ]
    acmi_response = response_payload(
        user_john, marie_acmi_request, 'Boening 767', early_payment=True, contract=john_contract, routes=routes)
    response = client.post(reverse('acmiresponse-list'), data=acmi_response, content_type='application/json')
    assert response.status_code == 201

    acmi_response_instance = ACMIResponse.objects.last()
    response = client.get(reverse('acmiresponse-list'))

    assert len(response.data) == 1
    assert response.data[0]['id'] == acmi_response_instance.pk


def test_user_should_be_able_to_delete_his_own_request(
    user_john, john_authenticated, client, john_acmi_request
):
    assert ACMIRequest.objects.count() == 1
    response = client.delete(
        reverse('acmirequest-detail', kwargs={'pk': john_acmi_request.pk})
    )
    assert response.status_code == 204
    assert ACMIRequest.objects.count() == 0


def test_user_should_not_be_able_to_delete_his_own_request_with_accepted_response(
    user_marie, marie_authenticated, client, marie_request_with_accepted_johns_response
):
    """
    Needed:
        I need maries request with an accepted johns response
        # maries_request_with_accepted_johns_response
    """
    response = client.delete(
        reverse('acmirequest-detail', kwargs={'pk': marie_request_with_accepted_johns_response.pk})
    )
    assert response.status_code == 400
    assert response.data[0] == 'You cannot delete this request that you already accepted a response to.'
    assert ACMIRequest.objects.count() == 1

# def test_user_should_be_able_to_edit_his_own_requests():

# def test_user_should_be_able_to_delete_his_own_requests():


# def user_should_be_able_to_delete_his_own_responses_only_if_the_response_hasnt_been_accepted():

