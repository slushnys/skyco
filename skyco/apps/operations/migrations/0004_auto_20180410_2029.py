# Generated by Django 2.0.4 on 2018-04-10 20:29

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('operations', '0003_auto_20180410_2028'),
    ]

    operations = [
        migrations.AlterField(
            model_name='flight',
            name='expires_at',
            field=models.DateTimeField(default=datetime.datetime(2018, 4, 10, 23, 29, 7, 8441, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='plane',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
