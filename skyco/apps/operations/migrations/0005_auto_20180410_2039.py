# Generated by Django 2.0.4 on 2018-04-10 20:39

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('operations', '0004_auto_20180410_2029'),
    ]

    operations = [
        migrations.RenameField(
            model_name='flight',
            old_name='id',
            new_name='uuid',
        ),
        migrations.AlterField(
            model_name='flight',
            name='created',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='flight',
            name='date_to',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='flight',
            name='expires_at',
            field=models.DateTimeField(default=datetime.datetime(2018, 4, 10, 23, 39, 40, 384333, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='flight',
            name='modified',
            field=models.DateTimeField(null=True),
        ),
    ]
