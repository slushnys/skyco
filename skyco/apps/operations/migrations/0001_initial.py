import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc
import uuid

import django.core.validators
import django.db.models.deletion


def forwards(apps, schema_editor):
    """
    This datamigration was generated automatically from a production Database

    The yaml dumps can be generated with the following commands:

    python/manage.py dump_shipping_method_price > shipping_method_price.yaml
    python/manage.py dump_shipping_method > shipping_method.yaml
    """
    Plane = apps.get_model('operations', 'Plane')
    Airport = apps.get_model('operations', 'Airport')
    Contract = apps.get_model('operations', 'Contract')

    Company = apps.get_model('users', 'Company')

    Plane.objects.bulk_creat({
        Plane(model='Airbus 320', registration_number='testregistrationnr1', operator=Company.objects.get(pk=1)),
        Plane(model='Boeing 747', registration_number='testregistrationnr2', operator=Company.objects.get(pk=1)),
        Plane(model='Airbus 310', registration_number='testregistrationnr3', operator=Company.objects.get(pk=1)),
    })

    Airport.objects.bulk_create([
        Airport(code='LAX'),
        Airport(code='AMS'),
        Airport(code='VLN'),
        Airport(code='EHV'),
    ])
    user = User.objecst.get(email='development@skyco.app')
    Contract.objects.create(user=user)


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('users', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Airport',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('code', models.CharField(max_length=4)),
            ],
        ),
        migrations.CreateModel(
            name='Flight',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('passenger_number', models.IntegerField()),
                ('date_from', models.DateField()),
                ('time_from', models.TimeField()),
                ('date_to', models.DateTimeField()),
                ('time_to', models.TimeField()),
                ('requested_arrival', models.DateTimeField()),
                ('call_sign', models.CharField(max_length=10)),
                ('created', models.DateTimeField()),
                ('modified', models.DateTimeField()),
                ('active', models.BooleanField(default=True)),
                ('expires_at', models.DateTimeField(default=datetime.datetime(2018, 4, 10, 22, 34, 36, 451855, tzinfo=utc))),
                ('deleted', models.BooleanField(default=False)),
                ('from_airport', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='from_airport', to='operations.Airport')),
            ],
        ),
        migrations.CreateModel(
            name='Plane',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('model', models.CharField(max_length=50)),
                ('registration_id', models.CharField(max_length=15)),
                ('operator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.Company')),
            ],
        ),
        migrations.AddField(
            model_name='flight',
            name='plane',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='operations.Plane'),
        ),
        migrations.AddField(
            model_name='flight',
            name='requestor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='requestor', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='flight',
            name='responders',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='flight',
            name='to_airport',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='to_airport', to='operations.Airport'),
        ),
    ]
