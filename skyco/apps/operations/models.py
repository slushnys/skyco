
from django.utils import timezone
from django.db import models

from skyco.apps.users.models import Company, User
from skyco.apps.operations.exceptions import RequestHasAcceptedResponse, ResponseAcceptedException


class Airport(models.Model):
    code = models.CharField(max_length=4)

    def __str__(self):
        return '{}'.format(self.code)


class PlaneType:
    ANY = 'ANY'
    A320 = 'A320'
    PLANE_TYPE_CHOICES = (
        (ANY, 'Any'),
        (A320, 'Airbus 320'),
    )


class Plane(models.Model):
    model = models.CharField(choices=PlaneType.PLANE_TYPE_CHOICES, max_length=50, blank=False)
    registration_id = models.CharField(max_length=15, blank=False)
    operator = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.model)


class ACMIRequestManager(models.Manager):

    def personal(self, user):
        """
        Return the personal ACMI requests of this user
        """
        return self.filter(user=user, deleted=False)


class ACMIRequest(models.Model):
    objects = ACMIRequestManager()
    # Request details
    plane = models.CharField(choices=PlaneType.PLANE_TYPE_CHOICES, max_length=50, blank=True)
    active = models.BooleanField(default=True)

    # Relationships
    user = models.ForeignKey(User, related_name='requests', on_delete=models.PROTECT)

    # Timing details
    expires_at = models.DateTimeField(null=True)

    deleted = models.BooleanField(default=False)
    deleted_at = models.DateTimeField(null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    viewed_at = models.DateTimeField(null=True)

    def __str__(self):
        return 'ACMI Request {}'.format(self.pk)

    def __unicode__(self):
        return

    def accept_response(self, response_instance: 'ACMIResponse'):
        response_instance.accepted = True
        return response_instance.save(update_fields=['accepted'])

    def mark_deleted(self):
        if self.responses.filter(accepted=True):
            raise RequestHasAcceptedResponse
        self.deleted = True
        self.deleted_at = timezone.now()
        self.save(update_fields=['deleted', 'deleted_at'])


class ACMIResponseManager(models.Manager):

    def to_personal_requests(self, user):
        return self.filter(acmi_request__user=user)


class ACMIResponse(models.Model):
    objects = ACMIResponseManager()
    # Response details
    price_block_hour = models.FloatField(help_text='Price per block hour')
    daily_allowance = models.FloatField(help_text='Per diem')
    min_block_hour = models.FloatField(help_text='Minimum block hour amount')
    plane = models.CharField(max_length=50, blank=False)
    early_payment = models.BooleanField(default=False)
    base = models.CharField(max_length=50)
    accepted = models.BooleanField(default=False)

    # Relationships
    acmi_request = models.ForeignKey(ACMIRequest, related_name='responses', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='responses', on_delete=models.PROTECT)
    contract = models.ForeignKey('Contract', related_name='responses', on_delete=models.PROTECT)

    deleted = models.BooleanField(default=False)
    deleted_at = models.DateTimeField(null=True)

    # Timing details
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    viewed_at = models.DateTimeField(null=True)

    def mark_deleted(self):
        if self.accepted:
            raise ResponseAcceptedException
        self.deleted = True
        self.deleted_at = timezone.now()
        self.save(update_fields=['deleted', 'deleted_at'])


class RouteType:
    LIVE = 'LIVE'
    FERRY = 'FERRY'
    ROUTE_TYPE_CHOICES = (
        (LIVE, 'live'),
        (FERRY, 'ferry'),
    )


class Route(models.Model):
    # Flight details
    departure = models.CharField(max_length=10, null=False)
    arrival = models.CharField(max_length=10, null=False)
    departure_at = models.DateTimeField()
    arrival_at = models.DateTimeField()
    passenger_number = models.IntegerField(null=False, help_text='PAX')
    call_sign = models.CharField(max_length=20, null=True)
    type = models.CharField(choices=RouteType.ROUTE_TYPE_CHOICES, max_length=50, blank=False, default=RouteType.LIVE)

    # Relationships
    acmi_request = models.ForeignKey(ACMIRequest, null=True, related_name='routes', on_delete=models.CASCADE)
    acmi_response = models.ForeignKey(ACMIResponse, null=True, related_name='routes', on_delete=models.CASCADE)

    # Timing details
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{departure} to {arrival}'.format(departure=self.departure, arrival=self.arrival)


class Contract(models.Model):
    name = models.CharField(max_length=30, blank=False)
    default_conditions = models.TextField(editable=False, default='Default contract text and conditions')
    additional_conditions = models.TextField()

    # Relationships
    user = models.ForeignKey(User, null=False, related_name='contracts', on_delete=models.CASCADE)

    # Timing details
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
