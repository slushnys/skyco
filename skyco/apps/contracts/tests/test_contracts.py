import json
from django.urls import reverse
from skyco.apps.operations.models import Contract


def test_user_should_be_able_to_create_a_contract(
    john_authenticated, client,
):
    payload = {
        'name': 'First contract',
        'additional_conditions': '<h1>Heading of conditions</h1>'
    }
    response = client.post(reverse('contract-list'), data=json.dumps(payload), content_type='application/json')
    assert response.status_code == 201
    assert Contract.objects.count() == 1


def test_user_should_not_be_able_to_create_a_contract_if_try_to_overwrite_default_conditions(
    john_authenticated, client,
):
    default_condition_overwritten = 'Im a hacker'
    payload = {
        'name': 'First contract',
        'default_conditions': default_condition_overwritten,
        'additional_conditions': '<h1>Heading of conditions</h1>'
    }
    response = client.post(reverse('contract-list'), data=json.dumps(payload), content_type='application/json')

    assert response.status_code == 201
    assert Contract.objects.count() == 1
    assert response.data['default_conditions'] != default_condition_overwritten
    assert response.data['default_conditions'] == Contract._meta.get_field('default_conditions').default


def test_user_can_view_his_contract(john_contract, user_john, john_authenticated, client):
    response = client.get(reverse('contract-detail', kwargs={'pk': john_contract.pk}))
    assert response.status_code == 200
    assert response.data['id'] == john_contract.pk


def test_user_should_be_able_to_edit_his_own_contracts(
    john_contract, user_john, john_authenticated, client
):
    assert john_contract.name == 'Johns first contract'
    new_contract_name = 'Johns second contract'
    response = client.put(
        reverse('contract-detail', kwargs={'pk': john_contract.pk}),
        data=json.dumps(
            {
                'name': new_contract_name,
                'additional_conditions': john_contract.additional_conditions
            }),
        content_type='application/json'
    )
    assert response.status_code == 200
    john_contract.refresh_from_db()
    assert john_contract.name == new_contract_name


def test_user_should_be_able_to_delete_his_contract(
    john_contract, user_john, john_authenticated, client
):
    client.delete(
        reverse('contract-detail', kwargs={'pk': john_contract.pk}),
    )
    assert Contract.objects.count() == 0
