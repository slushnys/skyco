from rest_framework import serializers
from skyco.apps.operations.models import Contract


class ContractSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Contract
        fields = (
            'id',
            'name',
            'user',
            'default_conditions',
            'additional_conditions'
        )
