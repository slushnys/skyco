from rest_framework import viewsets, permissions
from skyco.apps.operations.models import Contract
from skyco.apps.contracts.serializers import ContractSerializer


class ContactViewset(viewsets.ModelViewSet):
    model = Contract
    queryset = Contract.objects.none()
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ContractSerializer

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)
