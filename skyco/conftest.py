import os
import pytest


os.environ['IS_RUNNING_TESTS'] = 'True'


@pytest.fixture
def client():
    from django.test import Client
    return Client()


@pytest.fixture
def user_john():
    from skyco.apps.users.models import User
    return User.objects.create_user(username='john_doe', email='john@doe.com', password='johndoe')


@pytest.fixture
def user_marie():
    from skyco.apps.users.models import User
    return User.objects.create_user(username='marie_doe', email='marie@doe.com', password='mariedoe')


@pytest.fixture
def john_authenticated(user_john, client):
    client.login(username='john@doe.com', password='johndoe')


@pytest.fixture
def marie_authenticated(user_marie, client):
    client.login(username=user_marie.email, password='mariedoe')


@pytest.fixture
def john_contract(user_john):
    from skyco.apps.operations.models import Contract
    return Contract.objects.create(name='Johns first contract', additional_conditions='Not really much', user=user_john)
